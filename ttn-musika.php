<?php
/**
 * Plugin Name: TTN Musika
 * Description: TTN Musika Sistema
 * Version: 0.1
 * Author: Jimakker
 * Author URI: http://twitter.com/jimakker
 */


require_once('models/taldeak.php');
require_once('models/diskak.php');
require_once('models/zigiluak.php');
require_once('models/aretoak.php');
require_once('models/estiloak.php');
require_once('models/kontzertuak.php');
require_once('lib/MusikaClass.php');


$TTNMusika = TTN_Musika::getInstance();
