<?php

class TTN_Musika {

    private static $_instance = null;

    private function __construct() {}
    private function __clone() {}
    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    function getUpcomingEvents($service, $id, $page=1, $size=30 ){

      $args = array(
        'order'=>'ASC',
        'orderby' => 'meta_value_num',
        'meta_key'=>'data',
        'posts_per_page'	=> $size,
        'offset'	=> ($page-1)*$size,
        'post_type'		=> 'kontzertuak',
        'meta_query' => array(
            array(
                'key' => 'data',
                'value' => time(),
                'compare' => '>',
                'type' => 'NUMERIC'
            ),
        )
      );

      if($service && $id && $service !== 'all'){
        $args['meta_query'][] = array(
            'key' => $service,
            'value' => '"' . $id . '"',
            'compare' => 'LIKE'
        );
      }

      return get_posts($args);
    }
}
