<?php
if ( ! function_exists('kontzertuak_post_type') ) {

function kontzertuak_post_type() {

  $labels = array(
    'name'                => _x( 'Kontzertuak', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'Kontzertua', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'           => __( 'Kontzertuak', 'text_domain' ),
    'parent_item_colon'   => __( 'Kontzertu gurasoa', 'text_domain' ),
    'all_items'           => __( 'Kontzertua guztiak', 'text_domain' ),
    'view_item'           => __( 'Kontzertua ikusi', 'text_domain' ),
    'add_new_item'        => __( 'Kontzertua gehitu', 'text_domain' ),
    'add_new'             => __( 'Kontzertu berria', 'text_domain' ),
    'edit_item'           => __( 'Kontzertua editatu', 'text_domain' ),
    'update_item'         => __( 'Kontzertua eguneratu', 'text_domain' ),
    'search_items'        => __( 'Kontzertuak bilatu', 'text_domain' ),
    'not_found'           => __( 'Ez da kontzerturik aurkitu', 'text_domain' ),
    'not_found_in_trash'  => __( 'Ez dago kontzerturik zaborrontzian', 'text_domain' ),
  );
  $rewrite = array(
    'slug'                => 'kontzertuak',
    'with_front'          => true,
    'pages'               => true,
    'feeds'               => true,
  );
  $args = array(
    'label'               => __( 'kontzertua', 'text_domain' ),
    'description'         => __( 'kontzertuen informazio orriak', 'text_domain' ),
    'labels'              => $labels,
    'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => '',
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'rewrite'             => $rewrite,
    'capability_type'     => 'page',
  );

  register_post_type( 'kontzertuak', $args );
}

add_action( 'init', 'kontzertuak_post_type', 0 );
}

?>
