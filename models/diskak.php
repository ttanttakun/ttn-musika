<?php
if ( ! function_exists('diskak_post_type') ) {

// Register Custom Post Type
function diskak_post_type() {

  $labels = array(
    'name'                => _x( 'Diskak', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'Diska', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'           => __( 'Diskak', 'text_domain' ),
    'parent_item_colon'   => __( 'Diska gurasoa', 'text_domain' ),
    'all_items'           => __( 'Diska guztiak', 'text_domain' ),
    'view_item'           => __( 'Diska ikusi', 'text_domain' ),
    'add_new_item'        => __( 'Diska gehitu', 'text_domain' ),
    'add_new'             => __( 'Diska berria', 'text_domain' ),
    'edit_item'           => __( 'Diska editatu', 'text_domain' ),
    'update_item'         => __( 'Diska eguneratu', 'text_domain' ),
    'search_items'        => __( 'Diskak bilatu', 'text_domain' ),
    'not_found'           => __( 'Ez da diskarik aurkitu', 'text_domain' ),
    'not_found_in_trash'  => __( 'Ez dago diskarik zaborrontzian', 'text_domain' ),
  );
  $rewrite = array(
    'slug'                => 'diskak',
    'with_front'          => true,
    'pages'               => true,
    'feeds'               => true,
  );
  $args = array(
    'label'               => __( 'diska', 'text_domain' ),
    'description'         => __( 'Disken informazio orriak', 'text_domain' ),
    'labels'              => $labels,
    'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => '',
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'rewrite'             => $rewrite,
    'capability_type'     => 'page',
  );
  register_post_type( 'diskak', $args );

}

// Hook into the 'init' action
add_action( 'init', 'diskak_post_type', 0 );

}

?>