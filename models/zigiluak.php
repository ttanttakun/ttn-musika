<?php 

if ( ! function_exists('zigiluak_post_type') ) {

// Register Custom Post Type
function zigiluak_post_type() {

  $labels = array(
    'name'                => _x( 'Zigiluak', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'Zigilua', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'           => __( 'Zigiluak', 'text_domain' ),
    'parent_item_colon'   => __( 'Zigilu gurasoa', 'text_domain' ),
    'all_items'           => __( 'Zigilu guztiak', 'text_domain' ),
    'view_item'           => __( 'Zigilua ikusi', 'text_domain' ),
    'add_new_item'        => __( 'Zigilua gehitu', 'text_domain' ),
    'add_new'             => __( 'Zigilu berria', 'text_domain' ),
    'edit_item'           => __( 'Zigilua editatu', 'text_domain' ),
    'update_item'         => __( 'Zigilua eguneratu', 'text_domain' ),
    'search_items'        => __( 'Zigiluak bilatu', 'text_domain' ),
    'not_found'           => __( 'Ez da zigilurik aurkitu', 'text_domain' ),
    'not_found_in_trash'  => __( 'Ez dago zigilurik zaborrontzian', 'text_domain' ),
  );
  $rewrite = array(
    'slug'                => 'zigiluak',
    'with_front'          => true,
    'pages'               => true,
    'feeds'               => true,
  );
  $args = array(
    'label'               => __( 'zigilua', 'text_domain' ),
    'description'         => __( 'Zigiluen informazio orriak', 'text_domain' ),
    'labels'              => $labels,
    'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => '',
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'rewrite'             => $rewrite,
    'capability_type'     => 'page',
  );
  register_post_type( 'zigiluak', $args );

}

// Hook into the 'init' action
add_action( 'init', 'zigiluak_post_type', 0 );

}

?>