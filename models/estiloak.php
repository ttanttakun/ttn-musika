<?php

// Register Custom Taxonomy
function estiloak_custom_tax_fn() {

	$labels = array(
		'name'                       => _x( 'Estiloak', 'Taxonomy General Name', 'ttn_musika' ),
		'singular_name'              => _x( 'Etiketa', 'Taxonomy Singular Name', 'ttn_musika' ),
		'menu_name'                  => __( 'Estiloak', 'ttn_musika' ),
		'all_items'                  => __( 'All Items', 'ttn_musika'),
		'parent_item'                => __( 'Parent Item', 'ttn_musika' ),
		'parent_item_colon'          => __( 'Parent Item:', 'ttn_musika' ),
		'new_item_name'              => __( 'New Item Name', 'ttn_musika' ),
		'add_new_item'               => __( 'Add New Item', 'ttn_musika' ),
		'edit_item'                  => __( 'Edit Item', 'ttn_musika' ),
		'update_item'                => __( 'Update Item', 'ttn_musika' ),
		'view_item'                  => __( 'View Item', 'ttn_musika' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'ttn_musika' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'ttn_musika' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'ttn_musika' ),
		'popular_items'              => __( 'Popular Items', 'ttn_musika' ),
		'search_items'               => __( 'Search Items', 'ttn_musika' ),
		'not_found'                  => __( 'Not Found', 'ttn_musika' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'estiloa', array( 'taldeak','diskak','zigiluak','aretoak','kontzertuak' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'estiloak_custom_tax_fn', 0 );
