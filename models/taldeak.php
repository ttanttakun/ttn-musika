<?php

if ( ! function_exists('taldeak_cpt') ) {

// Register Custom Post Type
function taldeak_cpt() {

  $labels = array(
    'name'                => _x( 'Taldeak', 'Post Type General Name', 'taldeak_domain' ),
    'singular_name'       => _x( 'Taldea', 'Post Type Singular Name', 'taldeak_domain' ),
    'menu_name'           => __( 'Taldeak', 'taldeak_domain' ),
    'parent_item_colon'   => __( 'Talde gurasoa:', 'taldeak_domain' ),
    'all_items'           => __( 'Talde guztiak', 'taldeak_domain' ),
    'view_item'           => __( 'Taldea ikusi', 'taldeak_domain' ),
    'add_new_item'        => __( 'Talde berria gehitu', 'taldeak_domain' ),
    'add_new'             => __( 'Talde berria', 'taldeak_domain' ),
    'edit_item'           => __( 'Taldea editatu', 'taldeak_domain' ),
    'update_item'         => __( 'Taldea eguneratu', 'taldeak_domain' ),
    'search_items'        => __( 'Taldeak bilatu', 'taldeak_domain' ),
    'not_found'           => __( 'Ez da talderik aurkitu', 'taldeak_domain' ),
    'not_found_in_trash'  => __( 'Ez dago talderik zaborrontzian', 'taldeak_domain' ),
  );
  $args = array(
    'label'               => __( 'taldeak', 'taldeak_domain' ),
    'description'         => __( 'Taldeen informazio orriak', 'taldeak_domain' ),
    'labels'              => $labels,
    'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields', ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => '',
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'page',
  );
  register_post_type( 'taldeak', $args );

}

// Hook into the 'init' action
add_action( 'init', 'taldeak_cpt', 0 );

}

?>