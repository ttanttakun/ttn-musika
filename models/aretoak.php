<?php

if ( ! function_exists('aretoak_post_type') ) {

// Register Custom Post Type
function aretoak_post_type() {

  $labels = array(
    'name'                => _x( 'Aretoak', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'Aretoa', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'           => __( 'Aretoak', 'text_domain' ),
    'parent_item_colon'   => __( 'Areto gurasoa', 'text_domain' ),
    'all_items'           => __( 'Areto guztiak', 'text_domain' ),
    'view_item'           => __( 'Aretoa ikusi', 'text_domain' ),
    'add_new_item'        => __( 'Aretoa gehitu', 'text_domain' ),
    'add_new'             => __( 'Areto berria', 'text_domain' ),
    'edit_item'           => __( 'Aretoa editatu', 'text_domain' ),
    'update_item'         => __( 'Aretoa eguneratu', 'text_domain' ),
    'search_items'        => __( 'Aretoak bilatu', 'text_domain' ),
    'not_found'           => __( 'Ez da aretorik aurkitu', 'text_domain' ),
    'not_found_in_trash'  => __( 'Ez dago aretorik zaborrontzian', 'text_domain' ),
  );
  $rewrite = array(
    'slug'                => 'aretoak',
    'with_front'          => true,
    'pages'               => true,
    'feeds'               => true,
  );
  $args = array(
    'label'               => __( 'aretoa', 'text_domain' ),
    'description'         => __( 'Aretoen informazio orriak', 'text_domain' ),
    'labels'              => $labels,
    'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => '',
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'rewrite'             => $rewrite,
    'capability_type'     => 'page',
  );
  register_post_type( 'aretoak', $args );

}

// Hook into the 'init' action
add_action( 'init', 'aretoak_post_type', 0 );

}

?>